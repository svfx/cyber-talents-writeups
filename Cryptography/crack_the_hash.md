**Day:** 2

**Challenge:** 3

**Category:** Cryptography

**Level:** Basic

My second challenge for day 2 was on cryptography. A sector in security that always bedazzled me from when I found out what Caesar Cipher is and all sorts of other ciphers. 


To keep it short, here's the challenge:

![Crack The Hash](/Cryptography/imgs/Selection_127.png)

So, I'm not sure if this is a bonus point or a negative point, but I started by checking the length of the hash to know what type of hash I'm dealing with. 

Since there are 32 characters (MD5's hash encoding is 32 characters!) so I just tried it on different MD5 decoders (some websites did not yield results) et voilà! 


