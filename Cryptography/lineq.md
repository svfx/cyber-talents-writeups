**Day:** 11

**Challenge:** 13

**Category:** Crytography

**Level:** Medium

Today's challenge:

![challenge](/Cryptography/imgs/Selection_176.png)

You can find the text [here](/Cryptography/scripts/lineq.txt).

So the hint suggests that we'll be looking into the inverse of modulus.

In other words, if we take a look at the file, we can conclude this mathematical equation: ```(a * x) % b = c``` where we solve for ```x```.

The script can be found [here](/Cryptography/scripts/lineq.py).

The part that took time is the math for the inverse (in order to figure out which variable goes where).

But in the end, totally worth it!

**Flag:** FLAG{M0DUL4R_4LG3BR4_EeZ}
