#!/usr/bin/python3

from Crypto.Util.number import inverse

flag = []

with open('lineq.txt','r') as f:
	cnt = 0
	for line in f:
		# print (line)
		try:
			a = (line.strip().split('*'))[0]
			c = line.strip().split(' = ')[-1]
			b = line.strip().split(' mod ')[-1].split(' = ')[0]
			x = (inverse(int(a), int(b)) * int(c)) % int(b)
			flag.append(chr(x%256))
		except	ValueError:
			print ("flag in last line")

print (''.join(flag))		
