**Day:** 5

**Challenge:** 6

**Category:** Digital Forensics

**Level:** Easy


Today's challenge:

![challenge](/Digital%20Forensics/imgs/Selection_146.png)

Unzipping the file, I found a pcap (an API for capturing network traffic) file. First instinct? WIRESHARK!

Going through the packets, I start seeing messages:

![first-msg](/Digital%20Forensics/imgs/Screenshot%20from%202020-04-17%2000-53-25.png)

And from there, we can see the conversation was as follows:

```
A: Hey bro
B: Sup supp, are we ready
A: yeah, u got the files?
B: yes but i think the channel is not secured
A: the UTM will block the file transfer as the DLP module is active
B: ok we can use cryptcat
A: ok what the password then
B: let it be P@ssawordaya
A: hhh, ok
A: listen on 7070 and ill send you the file , bye
B: bye
```

So I ```tcp.port == 7070``` and start looking for more clues, and so I find this:

![port-7070](/Digital%20Forensics/imgs/Screenshot%20from%202020-04-17%2001-12-40.png)

The "PSH" flag or the start of data exchange. Given that it is expected there will be an exchange, I figured, maybe follow the stream. So right click > follow > TCP Stream > Save As (I went with hex dump because I first thought I'll directly be using it with cryptcat, but then found out you can save as raw too).


The next few steps required a little research. I learned about netcat, referred to as "Wireshark's companion", because it monitors and manages the flow of network traffic. 

For the next step we need 2 terminals:

1. to establish a simple server using my local machine with the port number and hex dump provided from the PCAP file. (netcat = host)

    ```netcat localhost 7070 < [hex-dump-file]```


2.  to listen and get decrypted picture (this one runs first)

    ```cryptcat -l -k P@ssawordaya -p 7070 > [output-file]```

And just like that!

![output](/Digital%20Forensics/imgs/Selection_148.png)

Now for the cherry on top. The challenge says the flag is the MD5 of the picture.

So one last terminal and ```md5sum [output-file]```.

The MD5/key/flag is "3beef06be834f3151309037dde4714ec"!

Until next flag!
