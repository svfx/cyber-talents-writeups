**Day:** 8

**Challenge:** 10

**Category:** Digital Forensics

**Level:** Medium

Today's challenge:

![challenge](/Digital%20Forensics/imgs/Selection_166.png)

When I unzipped the file, I found a compressed file with the the extention "ARC" by using ```file f100```; f100 is the name of the ARC file.

And the challenge says "highly compressed", so I'm guessed I need to keep track of the files I'm unzipping and their types?

So far? Case 0 was a zip file, case 1 was an  ARC file. And from there it's a straightforward process of check file type and extract based on type. 

On reaching the final file (.rar), there were files that contained a couple of links. 

So naturally, I had to check them to see if they'd lead me to a clue or flag.

Here's the link that got me through: https://gist.github.com/anonymous/ac2ce167c3d2c1170efe

The content is Obfuscated JavaScript*. So  just find an online compiler and THAT'S when we get the flag!


**Flag:** s-e-c-r-e-t----f-l-a-g-{-D-i-d-Y-o-u-R-e-a-l-l-y-F-i-n-d-M-e-}-	

*awesome resource: https://dzone.com/articles/obfuscation-what-is-obfuscation-in-javascript-why