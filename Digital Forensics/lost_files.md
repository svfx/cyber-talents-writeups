**Day:** 14

**Challenge:** 16

**Category:** Forensics

**Level:** Medium

Today's challenge:

![challenge](/Digital%20Forensics/imgs/Selection_185.png)

So I first checked the file type and got:

```lost_files.mem.001: DOS/MBR boot sector, code offset 0x58+2, OEM-ID "MSDOS5.0", sectors/cluster 2, reserved sectors 37, Media descriptor 0xf8, sectors/track 63, heads 255, hidden sectors 63, sectors 256977 (volumes > 32 MB), FAT (32 bit), sectors/FAT 996, rootdir cluster 7462, serial number 0x7fa1ddf, unlabeled```

After going through a few basics on how to analyze a file with the extension ```.001```, I used ```autopsy```.

In the File Analysis tab, I go through file contents and see this on a file called ```0xbb.doc.bak``` :

![content](/Digital%20Forensics/imgs/Screenshot%20from%202020-04-30%2001-57-30.png)

I tried looking for the through the directory ```Recycled``` and in the first file called ```_esktop.ini``` I found the flag in the content:

![flag](/Digital%20Forensics/imgs/Screenshot%20from%202020-04-30%2002-35-03.png)

