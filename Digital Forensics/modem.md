**Day:** 13

**Challenge:** 15

**Category:** Forensics

**Level:** Medium

Today's challenge was pretty easy for a "medium":

![challenge](/Digital%20Forensics/imgs/Selection_182.png)

All it took was 2 commands:

1. Since a file can use an extension and contain different contents, I wanted to know what I'm dealing with so,  ```file Adsl-modem.bin```

    ![res](/Digital%20Forensics/imgs/Selection_183.png)

2. ```unrar e Adsl-modem.bin``` to extract it

    ![flag](/Digital%20Forensics/imgs/Selection_184.png)

