**Day:** 2

**Challenge:** 2

**Category:** General Information

**Level:** Basic

Today's challenge was "basic" yet fundamental.

And since I just finished a course in CEH, I'd like to document why it is an imperative step in ethical hacking.

So according to Ric Messier, author of CEH v10 (the book I studied my course from), there are 5 main steps of ethical hacking.

1. Reconnaissance & Footprinting
2. Scanning & Enumeration
3. Gaining Access
4. Maintaining Access
5. Covering Tracks

This challenge falls under the very first stage or step. This step is all about gathering information from different resources about a certain target (reconnaissance). 

Though this might not be the exact case in this challenge, I believe it sort of is a foundational step to know how to find answers/information online. It might even come to you as a shock but this challenge was solved by almost 1/5th the number of actual tries!

![Business Gathering](/General%20Information/imgs/Selection_126.png)

Which with less than a minute of research, the answer is RSA Conference. "Where the world talks security", such an inspiring tagline!

To conclude, I'm loving the CTF personal challenge. I'm doing another one today! I'm hoping this challenge inspires others, too. 


