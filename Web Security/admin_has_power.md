**Day:** 1

**Challenge:** 1

**Category:** Web Security

**Level:** Easy


The challenge is to obtain administrator privileges on this login screen.

![Login Screen](/Web%20Security/imgs/Selection_117.png)

Being that it's a web security challenge, my first instinct was to inspect the source code of the page to look for any hard-coded credentials* (Ctrl + Shift + I), where I found the following:

![Login Credentials](/Web%20Security/imgs/Selection_118.png)

Et voilà! Logged in. But was that it? Was it really that simple? Nope.

![New Message](/Web%20Security/imgs/Selection_119.png)

So I inspect the HTML code again, y nada. However, inspection does not only mean elements (source code), it also means you can see sources (CSS/JavaScript/etc. files), Network, and Application (where you can see cookies). Yes, I took a tour.

I tried to rely on my curiosity before I looked for help online, since it's technically my first CTF. Hopefully by day 30 of this challenge, I rely on more than just curiosity! 

![Cookies of challenge URL](/Web%20Security/imgs/Selection_120.png)

So what first caught my eye was the word "role" and the value "support". So here's the awesome part, I just modified the value "support" to "admin" and refreshed the page on my browser and there it was! The flag.

![Challenge Flag](/Web%20Security/imgs/Selection_121.png)

**Bonus Lesson:** Found out how to add images to "*.md" => ! [] () **without spaces, where the the square brackets takes the alt text and the bracket takes the path of the image.**

*CWE-798: Use of hard-coded credentials (https://cwe.mitre.org/data/definitions/798.html)
