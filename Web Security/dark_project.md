**Day:** 9

**Challenge:** 11

**Category:** Web Security

**Level:** Medium


I took a 3 day break. This quarantine isn't making it easy to be productive.

BUT! Today's challenge is medium-leveled:

![challenge](/Web%20Security/imgs/Selection_169.png)

For web security challenges, I usually start with inspection, which didn't yield to anything this time.

So I looked at it from another angle:

![kali](/Web%20Security/imgs/Screenshot%20from%202020-04-23%2022-44-05.png)

The url may not tell us anything until we click on it. In other words, before opening the challenge link, I didn't know the the programming language used is PHP.

At this point I must say I have no background in PHP, so new strategy: what are my options? 

When I looked up PHP common attacks and visited my memory to enlist some of what I know/remember, I made a short list:

* **SQL Injection**: no results
* **Directory Traversal** : no results
* **Session Hijacking**: no results

**NOTE:** The list was obviously going to take time so I had to figure out a new strategy.

  *****

Directory traversal is something I've briefly read about, so I jumped into the rabbit-hole aka the internet to look for similar hacks or methods.

A while later, I find the mighty LFI and then found this awesome [cheat sheet] (https://highon.coffee/blog/lfi-cheat-sheet/).

What caught my eye was the ***PHP Wrapper php://filter***. It resemebled a pattern.

The pattern I'm referring to can be seen in the previous image, the "other angle". If we look at the end of the url it says, ```home=about```.

In this case, ```home``` is the ```page``` and ```about``` is the ```resource```.

Being that ```index``` is the skeleton of a page, I try the filter like so:

```index.php?home=php://filter/read=convert.base64-encode/resource=index```


AND FINALLY!

![progress](/Web%20Security/imgs/Screenshot%20from%202020-04-24%2000-11-52.png)

I inspect to get the full "text" and being that it's "base64 encoded", I just used an online decoder, and got the flag!

**Flag:** {pHp_Wr4P3rs_4r3_Us3fuL} 

