**Day:** 12

**Challenge:** 14

**Category:** Web Security

**Level:** Medium

Today's challenge:

![challenge](/Web%20Security/imgs/Selection_177.png)

At this point I think I've learned that there can be more to exploit in the actual website than in its inspection.

So while touring the website, I found:

![web](/Web%20Security/imgs/Selection_178.png)

So I tried writing a simple PHP script of **Hello world** with an extension of PDF (i.e. ```test.pdf```):

```
<?php

echo "Hello world!";

?>
```

Successful upload. I just had to keep tabs on the url to redirect myself to where the script is so I can test it.

And:

![tested](/Web%20Security/imgs/Selection_179.png)

#TODO: Must finish this writeup

