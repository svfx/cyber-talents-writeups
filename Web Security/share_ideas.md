**Day:** 7

**Challenge:** 9

**Category:** Web Security

**Level:** Medium

For this one, it took a little more time than usual to figure out.

![challenge](imgs/Selection_158.png)

The first thing I did was register an account and login to try and figure out what can be done as a registered user.

On inspection, I jumped to "storage" because if a user registers/logs in there's got to be a database that stores the information, right?

![db](imgs/Selection_160.png)

Invisible database.

Adrenaline rush!! I am now looking up SQL injections because I have just 1 questions on my mind:

**How can I test whether a DB is prone to SQL injections?**

Came across this on StackExchange (https://security.stackexchange.com/questions/67972/why-do-testers-often-use-the-single-quote-to-test-for-sql-injection).

Let's go!

So I tried "sharing" ```hello'``` and got this error:

![error](imgs/Selection_161.png)

I just tried copy pasting the error and found people asking about "SQLite error", and so I GUESS THIS MEANS I CAN INJECT THIS SQL-BASED DB WHAT NO WAY! 

Now, quick online revision on the different commands I can use. Note to self:

1. || is to concatenate an SQL command after the delimiter follows a string.
2. brackets are important and so are --
   
So I played around with the following commands:

1. Found this command online and thought to give it a try:

   ```hello' || (SELECT name FROM sqlite_master WHERE type='table' ORDER BY name));--```

   And I'm getting results!!! But what is ideas? I'm looking for a table that says users or shows that it may actually hold user information.

   ![res](imgs/Selection_162.png)


2. Then I tried to select all (```SELECT * from sqlite_master```) but got this error: 

    ```Error : HY000 1 only a single result allowed for a SELECT that is part of an expression ```


3. Found this answer online and can we praise the awesome people on StackOverflow (https://stackoverflow.com/questions/6460671/sqlite-schema-information-metadata)? So I tried:

    ```hello' || (SELECT sql FROM sqlite_master WHERE type='table'));--```

    **Note:** I removed the "order by" and on both the select name and select sql and actually got this:

    ![finally](imgs/Selection_164.png)

4. So I know what this means! One last command: 

    ```hello' || (SELECT password FROM xde43_users where role="admin"));--```

    And I'm in! Hello, flag.

    ![flag](imgs/Selection_165.png)