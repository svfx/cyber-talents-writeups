**Day:** 7

**Challenge:** 8

**Category:** Web Security

**Level:** Easy

So today's challenge is: this is Sparta!

I'm supposed to find the login password in this interface.

![sparta](imgs/Selection_153.png)

First instinct? What is the hint?

![hint](imgs/Selection_154.png)

Did it make any sense to me? Nope. So, I just inspected the site, looking for scripts.

And so I found THIS:

![script](imgs/Selection_155.png)

Tried to beautify it online so I could read it better via (https://beautifier.io/), but it literally just helped me read the code. Like really READ the code.

![code](imgs/Selection_157.png)

And I'm in!

![flag](imgs/Selection_156.png)

Since today marks a week of commitment on this challenge, I decided to do another one today! Medium-leveled flag :)